@extends('templates.main-layout')

@section('content')
    <div class="col-md-offset-3 col-sm-offset-2 col-xs-12 col-sm-8 col-md-6">
        <div class="home">
            <div class="logo text-center">
                <img src="{{ asset('images/logo.png') }}" alt="">
            </div>
            @include('partials.search-form')
        </div>
    </div>
@endsection