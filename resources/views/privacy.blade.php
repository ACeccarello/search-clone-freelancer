@extends('templates.main-layout')

@section('content')

<h2>Privacy Policy</h2>
<p>
Webcore1 owns and operates a number of different sites on the Web. Webcore1 is strongly committed to protecting the privacy of its user community. The intent of this privacy policy is to detail the information Webcore1 may gather about individuals who visit the Webcore1 Sites, how that information is used, and Webcore1's disclosure practices. Please note that this policy applies only to the Webcore1 Sites, and not to the Web sites of other companies or organizations to which we provide links or to any software that may be downloaded from the Webcore1 Sites.
</p>
<h2>Anonymous User Information</h2>
<p>
    Webcore1 collects some anonymous information each time you visit an Webcore1 Site so we can improve the overall quality of your online experience. We collect your IP address, referral data, and browser and platform type. You do not have to register with Webcore1 before we can collect this anonymous information.
</p>
<p>
    The Webcore1 Sites do not require you to share information that identifies you personally, such as your name or e-mail address, in order for you to use the Webcore1 Sites. The Webcore1 Sites assign an anonymous ID number to your requests and links the following additional data to that number: the date and time you visited the Webcore1 Site, your search terms, and the links upon which you choose to click. Like most standard Web site servers, we use log files to collect and store this anonymous user information. Webcore1 analyzes the information to examine trends, administer the site, track user's movement in the aggregate, and gather broad demographic information for aggregate use.
</p>
<h2>Individual User Information</h2>
<p>
    From time-to-time the Webcore1 Sites request information from you that may identify you personally in addition to the anonymous information described above. The requested information typically includes contact information (such as name, e-mail address, shipping address, and zip code), and demographic information (such as age, gender, occupation, and household income). Webcore1 will not use or disclose your individual user information except as described in this privacy policy.
</p>
<p>
    Webcore1 collects individual user information in the following ways from different Webcore1 Sites:
</p>
<h2>Surveys or Contests</h2>
<p>
    From time to time, Webcore1 may conduct surveys or contests on an Webcore1 Site. Participation in these surveys or contests is completely voluntary and you therefore have a choice whether or not to disclose individual user information to the Webcore1 Site. Contest information will be shared with the contest or survey sponsors to notify the winners and award prizes. Survey information will be used for purposes of monitoring or improving the use and satisfaction of the Webcore1 Site. We may use an intermediary to conduct these surveys or contests. These companies may use your personal information to help the Webcore1 Site communicate with you about offers from the Webcore1 Site and our marketing partners. Such third parties do not have Webcore1's permission to use your personally identifiable information for any secondary purpose.

</p>
<h2>Tell-A-Friend</h2>
<p>
    If you elect to use our referral service for informing a friend about any of our sites, we ask you for your friend's name and e-mail address. Webcore1 will automatically send the friend a one-time e-mail inviting them to visit the Webcore1 Site. Webcore1 does not store the information related to your friends and the one-time e-mail sent pursuant to your input will be our only contact with that person for such input.
</p>
<h2>Correspondence</h2>
<p>
    If you contact Webcore1, we may keep a record of that correspondence and we may collect your e-mail address.
</p>
<h2>Third Party Information Gathering</h2>

<p>
    The Webcore1 Sites include content and ads from third parties that may perform user activity tracking other than that described herein. For more information about each third party content provider, cookies, and how to "opt-out", please refer to such third party advertiser's privacy policy.
</p>
<p>
    The Webcore1 Sites contain links to other sites on the Web. Please be aware that Webcore1 is not responsible for the privacy practices of such other sites. We encourage our users to be aware when they leave our site and to read the privacy statements of each and every Web site that collects personally identifiable information. This privacy statement applies solely to information collected by this Web site.
</p>
<h2>Cookies</h2>
<p>
    Webcore1 may set and access Webcore1 or partner cookies on your computer. A cookie is a small data file that a Web site may send to your browser and which may then be stored on your system. We use both session cookies and persistent cookies. For the session cookie, once you close the browser, the cookie simply terminates. A persistent cookie is a small text file stored on your hard drive for an extended period of time. Persistent cookies can be removed by following Internet browser help file directions.
</p>
<p>
    We use cookie and web beacon technology to track which pages on the Webcore1 Sites our visitors view and which Web browsers our visitors use. We may also use cookies to help us provide personalized features and preferences to you within various Webcore1 Sites in a way that may be linked to individual user information that you elect to share with us. We also use "action tags" to assist in delivering the cookie. These action tags allow us to compile statistics and other data about our customers on an aggregate basis, by embedding a random identifying number on each user's browser. Then we track where that number shows up on our site.
</p>
<h2>Sharing</h2>
<p>
    Webcore1 may need to disclose the personal information we collect from you when required by law or otherwise necessary to comply with a current judicial proceeding, a criminal investigation, a court order, or legal process. We may also share your personal information if we believe it is necessary in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person or violations of our terms of use.
</p>
<p>
    To the extent Webcore1 use third party service providers or have co-sponsors of any survey, promotion or contents, the information we collect from you may be accessed or shared as part of the service or event with any such third party or co-sponsor, provided that any such disclosure will be subject to written agreement with such third party or co-sponsor to only use such information for the purpose of the service or event and to not share or make any other use of the information.
</p>
<h2>Mergers; Business Transitions</h2>
<p>
    In the event Webcore1 goes through a business transition, such as a merger, being acquired by another company, or selling a portion of its assets, users' personal information will, in most instances, be part of the assets transferred.
</p>
<h2>Choice/Opt-out</h2>
<p>
    We desire to keep you in control of the personal information you provide to us. Accordingly, you can review, correct, change or remove the personal registration information you provide to Webcore1 and that Webcore1 controls. In order to elect not to receive future communications from us, please Contact us
</p>
<h2>Notification of Changes</h2>
<p>
    If we decide to change our privacy policy, we will post those changes to this privacy statement in places we deem appropriate so our users are always aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it.
</p>
<p>
    If, however, we are going to use Individually Identifiable Information in a manner different from that stated at the time of collection we will notify users by posting a notice on our Web site for 30 days.
</p>
<h2>Special Notification with respect to Children's Privacy (Users under the age of 13)</h2>
<p>
    In response to concerns about protecting children's privacy online, Congress enacted the Children's Online Privacy Protection Act of 1998 ("COPPA"), which sets forth rules and procedures governing the ways in which Web sites may collect, use and disclose any personal information for children under the age of 13. In accordance with Webcore1 policy and COPPA regulations, we DO NOT:
</p>
<ul>
    <li>
        Request or knowingly collect personally identifiable information online or offline contact information from users under 13 years of age; or
    </li>
    <li>
        Knowingly use or share personal information from users under 13 years of age with third parties;
    </li>
</ul>
<p>
    It is possible that by fraud or deception we may receive information given to us or pertaining to children under 13. If we are notified of this, as soon as we verify the information, we will immediately delete the information from our servers
</p>
<p>
    Questions regarding children's privacy should be directed to: <a href="mailto:hello@youfind.online">hello@youfind.online</a>
</p>
@endsection