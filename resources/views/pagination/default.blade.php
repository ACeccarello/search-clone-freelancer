@if ($paginator->lastPage() > 1)
<ul class="pagination">
    @if ($paginator->currentPage() == $paginator->lastPage())
    <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
        <a href="{{ $paginator->previousPageUrl() }}">Previous</a>
    </li>
    @endif
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
            <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
        </li>
    @endfor
    @unless ($paginator->currentPage() == $paginator->lastPage())
    <li>
        <a href="{{ $paginator->url($paginator->currentPage()+1) }}" >Next</a>
    </li>
    @endunless
</ul>
@endif