<div id="search-form">
    <form action="/search" autocomplete="off">
        <div class="form-group {{ $errors->has('search') ? 'has-error' : '' }}">
            <div class="input-group">
              <input type="text" name="search" class="form-control" id="search" placeholder="What would you like to find?..."
              @if(!empty($searchQuery)) value="{{ $searchQuery }}" @endif>
              <span class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
              </span>
            </div><!-- /input-group -->
            {!! $errors->first('search', '<p class="help-block">:message</p>') !!}
        </div>
    </form>
</div>