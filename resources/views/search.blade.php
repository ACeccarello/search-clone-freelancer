@extends('templates.main-layout')

@section('content')
<!-- search bar -->
<div class="results-searchbar clearfix">
    <div class="col-xs-12 col-sm-8 col-md-6">
    @include('partials.search-form')
    </div>
</div>
<div class="col-xs-12 col-sm-8 col-md-6">
    @if($searchResults->isEmpty())
    <div class="result ">
        <p class="has-error"><strong>No result found, please enter your search word or phrase and hit submit again. </strong></p>
    </div>
    @else
    <h5>Your Search Results:</h5><hr>
    {!! ''; $i = 0; !!} {{-- counter for the suggestions to show in the second result  --}}
    @foreach($searchResults as $result)
    <div class="result">
        <h4><a target="_blank" href="{{ $result->url }}">{!! $result->title !!}</a></h4>
        <span class="dispurl"> {!! $result->dispurl !!} </span>
        <p>
            {!! $result->abstract !!}
        </p>
    </div>
    {!! ''; $i++; !!} {{-- counter increment --}}
    @endforeach
    @endif

</div>
@endsection
@section('suggestions')
@if(!empty($suggestions))
<div class="col-xs-12 col-sm-8 col-md-offset-1 col-md-5">
    <div class="suggestions">
        <h5>Explore more popular results</h5>
        <div class="results">
            <ul>
                @foreach ($suggestions as $suggestion)
                    <li> <a href="{{ url('/q/'.str_slug($suggestion->suggestion, '+' )) }}"> {!!  boldRelatedItems($searchQuery, $suggestion->suggestion) !!}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endif
@endsection
@section('pagination')
<div class="clearfix">
    <div class="col-xs-12 col-sm-8 col-md-6">
        @include('pagination.default', ['paginator' => $searchResults])
    </div>
</div>
@endsection