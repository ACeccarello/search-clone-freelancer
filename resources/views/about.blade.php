@extends('templates.main-layout')

@section('content')

<h2>About us: </h2>
<p>
YouFind is a fantastic alternative to the not-so-exciting search engines out there. We are committed to provide an amazing and fun experience for anyone who finds the colour RED attractive. Red is HOT, fun, bright, dangerous, and most importantly the colour of LOVE. Our search results are world class, using private and public APIs we aggregate the search results from leading search engines, and beautifully display them four you enjoyment.
</p>
<h2>Contact us: </h2>
<p>
    <a href="mailto:hello@youfind.online">hello@youfind.online</a>
</p>

@endsection