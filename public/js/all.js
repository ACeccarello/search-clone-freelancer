$(function () {
  $('#search').typeahead({
      ajax: {
          url: "related",
          timeout: 300,
          displayField: "suggestion",
          triggerLength: 3,
          method: "get",
          preDispatch: function (query) {
              return {
                  search: query
              }
          },
          preProcess: function (data) {
            var arr = [];
            var len = data.length;
            for (var i = 0; i < len; i++) {
                arr.push(data[i].suggestion);
            }
            if (data.success === false) {
                // Hide the list, there was some error
                return false;
            }
              // We good!
              return arr;
          }
      },
  });
});
//# sourceMappingURL=all.js.map