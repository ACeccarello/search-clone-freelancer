<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use App\Http\Requests;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('welcome');
    }

    /**
     * Display search results
     *
     * @return Response
     */
    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'search' => 'required',
        ]);
        $url = str_slug($request->input('search'),'+');
        return redirect('q/'.$url);
    }

    public function results(Request $request, $query = ' ') {

        $searchQuery = $query;
        $apiUrl = "https://yboss.yahooapis.com/ysearch/web";
        $args = [
            'q'      => $searchQuery,
            'format' => 'json'
        ];
        $results = getResultsFromYahooApi($apiUrl, $args);
        // Paginator, for a total of 5 pages, with 10 results each.
        $totalresults = 50;
        $itemsPerPage = 10;
        $numberofpages = $totalresults / $itemsPerPage;
        $currentPage = $request->input('page', 1);
        $offset = ( ($currentPage) * $itemsPerPage) - $itemsPerPage;
        if ($results->bossresponse->web->count > 0 ) {
            $searchResults = new LengthAwarePaginator(array_slice($results->bossresponse->web->results, $offset, $itemsPerPage, true), $totalresults, $itemsPerPage, $currentPage, ['path' => $request->url(), 'query' => $request->query()]);
        } else {
            // create an empty collection with 0 results.
            $searchResults = new LengthAwarePaginator(collect(), 0, $itemsPerPage, $currentPage);
        }
        $suggestions = $this->related($request, $query);
        $searchQuery = str_replace('+', ' ', $query);
        if($query == ' ') {
            $searchQuery = '';
        }
        return view('search', compact('searchResults', 'suggestions', 'searchQuery'));
    }
     /**
     * Return related results
     *
     * @return Response
     */
    public function related(Request $request, $query = '') {
        if(!empty($request->input('search'))) {
            $q = $request->input('search');
        } else {
            $q = $query;
        }

        $apiUrl = "https://yboss.yahooapis.com/ysearch/related";
        $args = [
            'q' => $q,
            'count' => 10,
            'format' => 'json'
        ];

        $results = getResultsFromYahooApi($apiUrl, $args);
        $results = $results->bossresponse->related->count > 0 ? $results->bossresponse->related->results : [];
        return $results;
    }
    /**
     * about static webpage
     * @return [type] [description]
     */
    public function about() {
        return view('about');
    }

    /**
     * terms of use static page
     * @return [type] [description]
     */
    public function terms() {
        return view('terms');
    }

    /**
     * privacy policy static page
     * @return [type] [description]
     */
    public function privacy() {
        return view('privacy');
    }
}
