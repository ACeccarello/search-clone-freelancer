<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'SearchController@index');
Route::get('search', 'SearchController@search');
Route::get('related/{query?}', 'SearchController@related');
Route::get('q/related/{query?}', 'SearchController@related');
Route::get('q/{query?}', 'SearchController@results');
Route::get('about', 'SearchController@about');
Route::get('privacy', 'SearchController@privacy');
Route::get('terms-of-use', 'SearchController@terms');
