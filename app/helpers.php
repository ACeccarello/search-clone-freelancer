<?php
    use App\Http\Controllers\OAuthConsumer;
    use App\Http\Controllers\OAuthRequest;
    use App\Http\Controllers\OAuthSignatureMethod_HMAC_SHA1;
    use App\Http\Controllers\OAuthUtil;


    function getResultsFromYahooApi($apiUrl, $args) {
        $cc_key  = env('YAHOO_BOSS_CCKEY');
        $cc_secret = env('YAHOO_BOSS_CCSECRET');

        $consumer = new OAuthConsumer($cc_key, $cc_secret);
        $yahooRequest = OAuthRequest::from_consumer_and_token($consumer, NULL,"GET", $apiUrl, $args);
        $yahooRequest->sign_request(new OAuthSignatureMethod_HMAC_SHA1(), $consumer, NULL);

        $apiUrl = sprintf("%s?%s", $apiUrl, OAuthUtil::build_http_query($args));
        $ch = curl_init();
        $headers = array($yahooRequest->to_header());
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $rsp = curl_exec($ch);
        // dd($rsp);
        $results = json_decode($rsp);
        return $results;
    }

    function boldRelatedItems($searchQuery, $suggestion) {
        $words = explode(" ", $searchQuery);
        foreach ($words as $word) {
            $suggestion = preg_replace('/\b'.$word.'\b/u',"<strong>$word</strong>",$suggestion);
        }
        return $suggestion;
    }